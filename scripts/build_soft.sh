# Ensure that required variables are set.
: ${CI_PROJECT_DIR:?"Please set environment variable CI_PROJECT_DIR with 'your soft' repository (absolute) path."}
: ${BUILD_MODE:?"Please choose build mode among configure, build, test values."}


# set default build dir
BUILD_DIR="${BUILD_DIR:=$CI_PROJECT_DIR/build}"

# set default config file
CONF_FILE="${CONF_FILE:=$CI_PROJECT_DIR/configs/default.cmake}"

case $BUILD_MODE in	
configure)		
echo "Configure in $BUILD_DIR";
cmake -S $CI_PROJECT_DIR -B $BUILD_DIR -DCONF_FILE=$CONF_FILE;;
build)
echo "Build in $BUILD_DIR";
cmake --build $BUILD_DIR;;
test)
echo "Tests in $BUILD_DIR";
ctest --test-dir $BUILD_DIR --output-junit test_results.xml;;
install)
echo "Install from $BUILD_DIR to $INSTALL_DIR";;
# cmake --install $BUILD_DIR
*)
echo "Error, unknown input mode";;
esac



