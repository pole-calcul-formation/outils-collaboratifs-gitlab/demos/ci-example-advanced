#[=======================================================================[.rst:
Findfftw3
-----------

Find fftw3 libraries and headers

Usage :
 
find_package(fftw3 REQUIRED COMPONENTS name ... OPTIONAL_COMPONENTS name ...)
target_link_libraries(yourlib PRIVATE fftw3::name)

with name among :
- fftw3
- mpi
- omp
- threads

for double precision.

And for other prec, with name among:
- fftw3,mpi,omp,threads + _long, _quad, _float

Set fftw3_ROOT=<where fftw3 is installed>
if it's not in a "classic" place or if you want a specific version

# Written by F. Pérignon, 2023
#]=======================================================================]

find_package(PkgConfig)

function(find_fftw_component)

  set(oneValueArgs NAME HEADER LIB)
  cmake_parse_arguments(comp "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  pkg_check_modules(PC_${comp_NAME} QUIET ${comp_NAME}) # May set PkgConfig::fftw3)
  
  find_path(fftw3_${comp_NAME}_INCLUDE_DIR NAMES ${comp_HEADER}
    PATHS ${PC_fftw3_INCLUDE_DIRS} ${${PC_${comp_NAME}}_INCLUDE_DIRS}
    )

  find_library(fftw3_${comp_NAME}_LIBRARY NAMES ${comp_LIB}
    PATHS ${PC_fftw3_LIBRARY_DIRS} ${${PC_${comp_NAME}}_LIBRARY_DIRS}
    )

  if(fftw3_${comp_NAME}_INCLUDE_DIR AND fftw3_${comp_NAME}_LIBRARY)
    set(fftw3_${comp_NAME}_FOUND ON PARENT_SCOPE)
  else()
    
    # cmake_print_variables(fftw3_${comp_NAME}_INCLUDE_DIR  fftw3_${comp_NAME}_LIBRARY)
  endif()

endfunction()


# -- Default and minimal is fftw3, double precision ---
# -- First step: try to use pkgconfig --


pkg_check_modules(PC_fftw3 fftw3)

foreach(_component IN LISTS fftw3_FIND_COMPONENTS)
  
  if(_component STREQUAL "fftw3")
    find_fftw_component(NAME fftw3 HEADER fftw3.h LIB fftw3)
  elseif(_component STREQUAL "fftw3_long")
    find_fftw_component(NAME fftw3_long HEADER fftw3l.f03 LIB fftw3l)
  elseif(_component STREQUAL "fftw3_float")
    find_fftw_component(NAME fftw3_float HEADER fftw3f.f03 LIB fftw3f)
  elseif(_component STREQUAL "fftw3_quad")
    find_fftw_component(NAME fftw3_quad HEADER fftw3q.f03 LIB fftw3q)
  elseif(_component STREQUAL "mpi")
    find_fftw_component(NAME mpi HEADER fftw3-mpi.f03 LIB fftw3_mpi)
  elseif(_component STREQUAL "mpi_long")
    find_fftw_component(NAME mpi_long HEADER fftw3l-mpi.f03 LIB fftw3l_mpi)
  elseif(_component STREQUAL "mpi_float")
    find_fftw_component(NAME mpi_float HEADER fftw3f-mpi.f03 LIB fftw3f_mpi)
  elseif(_componentonent STREQUAL "omp")
    find_fftw_component(NAME omp HEADER fftw3.f03 LIB fftw3_omp)
  elseif(_component STREQUAL "omp_long")
    find_fftw_component(NAME omp_long HEADER fftw3l.f03 LIB fftw3l_omp)
  elseif(_component STREQUAL "omp_float")
    find_fftw_component(NAME omp_float HEADER fftw3f.f03 LIB fftw3f_omp)
  elseif(_component STREQUAL "omp_quad")
    find_fftw_component(NAME omp_quad HEADER fftw3q.f03 LIB fftw3q_omp)
  elseif(_componentonent STREQUAL "threads")
    find_fftw_component(NAME threads HEADER fftw3.f03 LIB fftw3_threads)
  elseif(_component STREQUAL "threads_long")
    find_fftw_component(NAME threads_long HEADER fftw3l.f03 LIB fftw3l_threads)
  elseif(_component STREQUAL "threads_float")
    find_fftw_component(NAME threads_float HEADER fftw3f.f03 LIB fftw3f_threads)
  elseif(_component STREQUAL "threads_quad")
    find_fftw_component(NAME threads_quad HEADER fftw3q.f03 LIB fftw3q_threads)
  endif()
     
endforeach()


include(FindPackageHandleStandardArgs)
# cmake_print_variables(fftw3_REQUIRED_VARS fftw3_mpi_LIBRARY fftw3_mpi_LIBRARIES fftw3_mpi_CFLAGS fftw3_mpi_LDFLAGS fftw3_mpi_INCLUDE_DIR)

find_package_handle_standard_args(fftw3
  # REQUIRED_VARS fftw3_LIBRARY fftw3_INLCUDE_DIR
  HANDLE_COMPONENTS
  VERSION_VAR PC_fftw3_VERSION
  HANDLE_VERSION_RANGE
  )

# Hide variables from ccmake interface
mark_as_advanced(
  PC_fftw3_INCLUDE_DIR
  PC_fftw3_LIBRARY
)

foreach(_component IN LISTS fftw3_FIND_COMPONENTS)
  
  if(fftw3_${_component}_FOUND)
    if(NOT TARGET fftw3::${_component})
      add_library(fftw3::${_component} IMPORTED INTERFACE)
      set_property(TARGET fftw3::${_component} PROPERTY INTERFACE_LINK_LIBRARIES ${fftw3_${_component}_LIBRARY})
      if(fftw3_${_component}_INCLUDE_DIR)
        set_target_properties(fftw3::${_component} PROPERTIES
          INTERFACE_INCLUDE_DIRECTORIES "${fftw3_${_component}_INCLUDE_DIR}")
      endif()
    endif()
  else()
    set(fftw3_FOUND FALSE CACHE INTERNAL "")
  endif()
endforeach()

